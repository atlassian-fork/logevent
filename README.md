# logevent - A structured event logger abstraction #

We've moved to https://github.com/asecurityteam/logevent.

This repository is read-only and maintained to support existing users. New development will happen in the new location.
